﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : MonoBehaviour
{
	
	public float maxS = 11f;


	private Rigidbody2D rb2d = null;
	public bool saltar = false;
	private float move = 0f;
	private bool attack = false;
	private bool muerto = false;
	private Animator anim;
	private bool flipped = false;
	private Vector3 origen;
	public float jump;
	public bool insuelo = true;
	private Vector3 inipos; 


	// Use this for initialization
	void Awake()
	{
		origen = transform.position;


		rb2d = GetComponent<Rigidbody2D>();


		anim = GetComponent<Animator>();

		inipos.x = -7;
			inipos.y=-1;
			inipos.z= 2;
	}


	void FixedUpdate()
	{


		move = Input.GetAxis("Horizontal");
		if(Input.GetKeyDown(KeyCode.UpArrow)&& insuelo==true)
		{
			jump = 9;
			anim.SetBool ("Jump", true);
			insuelo = false;
		}
		else
		{
			jump= 0;

		}
		rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y+jump);
		if(Input.GetButton("Jump"))
		{
			attack = true;
			anim.SetBool("Attack", true);
		}

		else
		{
			anim.SetBool("Attack", false);
		}

		saltar = Input.GetKeyDown(KeyCode.UpArrow);




		if(rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f)
		{
			if((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))
			{
				flipped = !flipped;
				this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
			}
			anim.SetBool("Run", true);
		}
		else
		{
			anim.SetBool("Run", false);
		}
	}
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "muerte")
		{
			muerto = true;
			anim.SetBool("Dead", true);
			Invoke ("reset", 0.8f);
		}

		if (coll.gameObject.tag == "Suelo") {
			insuelo = true;
			anim.SetBool ("Jump", false);
		} 

	}

	void Restart()
	{
		this.transform.position = origen;
		anim.SetBool("Dead", false);
		muerto = false;
		anim.Play("idle_Knight");
	}

	void OnBecameInvisible(){
		transform.position = new Vector3(-7,-1,0);
	}

	void reset(){
		transform.position = inipos;
	}

}